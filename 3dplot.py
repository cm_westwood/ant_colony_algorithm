'''
https://www.jb51.net/article/122837.htm
'''
# 载入模块
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

# 生成数据
x = np.linspace(-6 * np.pi, 6 * np.pi, 1000)
y = np.sin(x)
z = np.cos(x)
plt.plot(x,y)
# # 创建 3D 图形对象
# fig = plt.figure()
# ax = Axes3D(fig)

# # 绘制线型图
# ax.plot(x, y, z, linewidth=10)

# # 显示图
plt.show()
