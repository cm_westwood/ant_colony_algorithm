# -*- coding: utf-8 -*-
"""
Created on Sat Mar 25 23:28:29 2017

@author: wyl
"""

import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import numpy as np
import math

plt.close()  # clf() # 清图 cla() # 清坐标轴 close() # 关窗口
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.axis("equal")  # 设置图像显示的时候XY轴比例
plt.grid(True)  # 添加网格
plt.ion()  # interactive mode on
print('开始仿真')
try:
    for t in range(180):
        x = np.random.randint(-100, 100)
        y = np.random.randint(-100, 100)
        plt.scatter(x, y)
        plt.pause(0.001)
        if t >= 100:
            plt.cla()
except Exception as err:
    print(err)
